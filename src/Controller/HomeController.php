<?php

namespace App\Controller;

use App\Entity\Customer;
use App\Entity\Message;
use App\Form\MessageType;

use App\Repository\CustomerRepository;
use App\Service\JsonService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    // home controller
    #[Route('/', name: 'home')]
    public function new(
        Request $request,
        CustomerRepository $customerRepository,
        EntityManagerInterface $em,
        JsonService $jsonService
    ): Response
    {
        $message = new Message();

        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $message = $form->getData();

            $senderCustomer = $customerRepository->findOneBy(['email' => $message->getCustomer()->getEmail()]);

            if ($senderCustomer != null) {
                $message->setCustomer($senderCustomer);
            }

            $em->persist($message);
            $em->flush();

            // make json file
            $jsonService->MakeAndSaveJson($message);

            return $this->redirectToRoute('home');

        }

        return $this->renderForm('home/index.html.twig', [
            'form' => $form
        ]);
    }
}
