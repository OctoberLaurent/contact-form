<?php

namespace App\Controller;

use App\Entity\Customer;
use App\Entity\Message;
use App\Repository\CustomerRepository;
use App\Repository\MessageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    // Administrator home
    #[Route('/admin', name: 'list')]
    public function list(CustomerRepository $customerRepository): Response
    {
        return $this->render('admin/list.html.twig', [
            'customers' => $customerRepository->findCustomersWhoseMessagesHaveNotBeenProcessed()
        ]);
    }

    // show single message
    #[Route('/admin/show/{id}', name: 'show')]
    public function show(Customer $customer): Response
    {
        return $this->render('admin/show.html.twig', [
            'customer' => $customer
        ]);
    }

    // validates the processing of the message
    #[Route('/admin/valid/{id}', name: 'processed')]
    public function processed(Message $message, EntityManagerInterface $entityManager): Response
    {
        $message->setIsProcessed(true);
        $idCustomer = $message->getCustomer()->getId();
        $entityManager->persist($message);
        $entityManager->flush();

        return $this->redirectToRoute('show', ['id' => $idCustomer ]);
    }

    // view of processed messages
    #[Route('/admin/processed-message', name: 'processed_message')]
    public function messageProcessed(MessageRepository $messageRepository): Response
    {

        return $this->render('admin/processed-message.html.twig', [
            'messages' => $messageRepository->findBy(['isProcessed' => true])
        ]);
    }
}
