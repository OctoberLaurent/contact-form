<?php

namespace App\Entity;

use App\Repository\MessageRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


#[ORM\HasLifecycleCallbacks()]
#[ORM\Entity(repositoryClass: MessageRepository::class)]
class Message
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[Assert\NotBlank]
    #[Assert\Length(min: 5, max: 800)]
    #[ORM\Column(type: 'text')]
    private $content;

    #[Assert\Valid]
    #[ORM\ManyToOne(targetEntity: Customer::class, inversedBy: 'message', cascade:['persist'])]
    private $customer;

    #[ORM\Column(type: 'boolean', nullable: true)]
    private $isProcessed;


    #[ORM\Column(type: 'datetime', nullable: true)]
    private $sendAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function getIsProcessed(): ?bool
    {
        return $this->isProcessed;
    }

    public function setIsProcessed(?bool $isProcessed): self
    {
        $this->isProcessed = $isProcessed;

        return $this;
    }

    public function getSendAt(): ?\DateTimeInterface
    {
        return $this->sendAt;
    }

    #[ORM\PrePersist]
    public function setSendAt(): self
    {
        $this->sendAt = new \DateTime();

        return $this;
    }
}
