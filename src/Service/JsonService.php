<?php

namespace App\Service;

use App\Entity\Message;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\KernelInterface;

class JsonService
{
    public function __construct(Filesystem $filesystem, KernelInterface $appKernel)
    {
        $this->filesystem = $filesystem;
        $this->projectRoot = $appKernel->getProjectDir();
    }
    /*
     *  Make json file for each demand
     */
    public function MakeAndSaveJson(Message $message) : void
    {
        $directory = $this->projectRoot.'/jsonFiles';
        $json = $this->makeJson($message);

        if (!$this->filesystem->exists($directory)){
            try {
                $this->filesystem->mkdir($directory, 0775);
                $this->filesystem->chown($directory, "www-data");
                $this->filesystem->chgrp($directory, "www-data");
            } catch (IOExceptionInterface $exception) {
                echo "An error occurred while creating your directory at ".$exception->getPath();
            }
        }

        try {

            $name = 'Message_'.$message->getCustomer()->getName().'_'.uniqid().'.json';
            $fileName = $directory.'/'.$name;
            $this->filesystem->touch($fileName);
            $this->filesystem->chmod($fileName, 0777);
            $this->filesystem->dumpFile($fileName, $json);

        } catch (IOExceptionInterface $exception) {
            echo "Error creating file at". $exception->getPath();
        }
    }

    /*
     * places the json in a string
     */
    private function makeJson(Message $message) : string
    {
        $arrayMessage = [
            'name' => $message->getCustomer()->getName(),
            'email' => $message->getCustomer()->getEmail(),
            'content' => $message->getContent(),
            'sendAt' => $message->getSendAt(),
        ];

        $json = json_encode($arrayMessage);

        return $json;
    }

}